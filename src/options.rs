pub use clap::Parser;

#[derive(Debug, Parser)]
pub struct Options {
    #[clap(short, long, default_value_t = 1_000_000)]
    pub cardinal: u64,

    pub choices: Vec<String>,
}
