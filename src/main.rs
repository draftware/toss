mod options;

use crate::options::{Options, Parser};
use indicatif::ProgressBar;
use rand::seq::SliceRandom;
use std::collections::hash_map::HashMap;

fn main() {
    let options = Options::parse();

    let mut results = HashMap::new();

    let progress_bar = ProgressBar::new(options.cardinal);
    for _ in 0..options.cardinal {
        *results
            .entry(
                options
                    .choices
                    .choose(&mut rand::thread_rng())
                    .expect("Failed to pick solution at random"),
            )
            .or_insert(0) += 1;

        progress_bar.inc(1);
    }
    progress_bar.finish();

    let winner = results
        .iter()
        .max_by(|lhs, rhs| lhs.1.cmp(&rhs.1))
        .expect("Failed to sort solutions")
        .0;

    let font = figlet_rs::FIGfont::standard().expect("Failed to generate font");
    let figure = font
        .convert(winner)
        .expect("Failed to generate ASCII figure");

    println!("{figure}")
}
