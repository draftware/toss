# Toss

Once upon a day, a couple of friend needed to pick between their two names for their heir to come.
Knowing how biased tossing a coin can be (for real, look it up), they asked for a more neutral solution.
To be fair, I've no idea how unbiased Rust's `rand` crate is. Hopefully more than an actual coin.

## Usage

```
Usage: toss --cardinal <CARDINAL> [CHOICES]...

Arguments:
  [CHOICES]...

Options:
  -c, --cardinal <CARDINAL>
  -h, --help                 Print help
```

### Example

To randomly pick between `foo`, `bar` and `baz` a thousand times and print whichever won:

```
toss -c 1000 foo bar baz
██████████████████████████████████████████████████████████████ 1000/1000
 | |__     __ _   _ __
 | '_ \   / _` | | '__|
 | |_) | | (_| | | |
 |_.__/   \__,_| |_|
```
